//
// Created by happycactus on 07/02/19.
//

#include "StrongType.h"
#include "StrongType/BitStreamTypes.h"

#include <type_traits>

#include <gtest/gtest.h>

#define STATIC_ASSERT(expr) static_assert(expr, #expr)

TEST(UtilsLimitedStrongTypeTest, SetGetValue)
{
    struct ITag {
    };
    using I = utils::StrongType<int, ITag, utils::validators::MinMaxValidator<int, 0, 100>>;

    I i;

    EXPECT_NO_THROW(i = I{10});
    EXPECT_EQ(i.value(), 10);

    EXPECT_THROW(i = I{110}, std::out_of_range);
    EXPECT_NO_THROW(i = I{0});
    EXPECT_THROW(i = I{-1}, std::out_of_range);
    EXPECT_NO_THROW(i = I{100});
    EXPECT_THROW(i = I{101}, std::out_of_range);

    using U = utils::StrongType<int, ITag>;

    EXPECT_NO_THROW(i = U{20});
    EXPECT_THROW(i = U{110}, std::out_of_range);
}
