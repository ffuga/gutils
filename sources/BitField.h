//
// Created by Federico Fuga on 19/11/18.
//

#ifndef BRIDGEMATE3_BITFIELD_H
#define BRIDGEMATE3_BITFIELD_H

#include <cstdint>
#include <type_traits>

namespace utils {

template<int N, typename C = int>
class BitField {
    C mValue;
public:
    BitField() : mValue()
    {}

    explicit BitField(C v) : mValue(v)
    {}

    C leftAligned() const
    {
        return mValue << (8 * sizeof(C) - N);
    }

    constexpr int bits() const
    { return N; }

    const C &value() const
    { return mValue; }

    C &value()
    { return mValue; }
};

template<int N, typename T>
auto makeBitFieldRef(T &var)
{
    return BitField<N, decltype(var) &>(var);
}

template<int N, typename T>
auto makeBitField(T var)
{
    return BitField<N, decltype(var)>(var);
}

}


#endif //BRIDGEMATE3_BITFIELD_H
