//
// Created by Federico Fuga on 12/06/18.
//

#ifndef GUTILS_DATASTREAMCOMMONS_H
#define GUTILS_DATASTREAMCOMMONS_H

#include "DataStream.h"
#include "StrongType.h"

namespace utils {

using VectorLenghtType = uint16_t;
using StringLengthType = uint8_t;

template<typename Endianness, typename T, typename TAG, typename Validator>
OutputDataStream<Endianness> &
operator<<(OutputDataStream<Endianness> &stream, const utils::StrongType<T, TAG, Validator> &v)
{
    return stream << v.value();
};

template<typename Endianness, typename T, typename TAG, typename Validator>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, utils::StrongType<T, TAG, Validator> &v)
{
    T t;
    stream >> t;
    v = utils::StrongType<T, TAG>(t);
    return stream;
};

template<typename E>
constexpr auto to_integral(E e) -> typename std::underlying_type<E>::type
{
    return static_cast<typename std::underlying_type<E>::type>(e);
}

template<typename Endianness, typename T, typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const T &v)
{
    return stream << to_integral(v);
};

template<typename Endianness, typename T, typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, T &v)
{
    typename std::underlying_type<T>::type t;
    stream >> t;
    v = T(t);
    return stream;
};

template<typename Endianness, template<typename, typename> class V, typename T, typename U>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const V<T, U> &container)
{
    stream << static_cast<VectorLenghtType>(container.size());

    for (const auto &v : container) {
        stream << v;
    }
    return stream;
};

template<typename Endianness, template<typename, typename> class V, typename T, typename U>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, V<T, U> &container)
{
    size_t s = stream.template get<VectorLenghtType>();
    for (size_t i = 0; i < s; ++i) {
        T t;
        stream >> t;
        container.push_back(t);
    }
    return stream;
};

template<typename Endianness>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const std::string &string)
{
    stream.template putString<StringLengthType>(string);
    return stream;
}

template<typename Endianness>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, std::string &string)
{
    string = stream.template getString<StringLengthType>();
    return stream;
}

template<typename U, typename V, typename Endianness>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const std::pair<U, V> &pair)
{
    stream << pair.first << pair.second;
    return stream;
}

template<typename U, typename V, typename Endianness>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, std::pair<U, V> &pair)
{
    stream >> pair.first >> pair.second;
    return stream;
}

}

#endif //GUTILS_DATASTREAMCOMMONS_H
