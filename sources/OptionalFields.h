/** @file OptionalFields.h
 * @brief Implements an "Optional fields management" for packets with interface to the DataStream system
 */
// Created by Federico Fuga on 05/06/18.
//

#ifndef BRIDGEMATE3_OPTIONALFIELDS_H
#define BRIDGEMATE3_OPTIONALFIELDS_H

#include "DataStreamExt.h"

#include <boost/optional.hpp>
#include <array>
#include <sstream>

namespace utils {

/** @brief An optional field implementation
 * @tparam T        The underlying type
 * @tparam IDX      The index in the bitmap
 *
 * This class implements a field that can be either set, with a value, or not set.
 * It makes use of boost::optional<T>. It offers the same functionalities, with other names for abstraction
 * purpose. Also, it encapsulate an Index field to distinguish different optionals with the same underlying type,
 * though it could be achieved using strong types instead.
 *
 */
template<typename T, int IDX>
class OptionalField {
    boost::optional<T> v;
public:
    OptionalField() : v()
    {}

    explicit OptionalField(T &&value) : v(std::forward(value))
    {}

    /**
     * @brief Assigns the underlying value with a value of the underlying type
     * @param value the value to assign
     * @return a reference to the same object
     */
    OptionalField<T, IDX> &operator=(const T &value)
    {
        v = value;
        return *this;
    };

    /**
     * @brief Assigns the underlying value with a value of the underlying type
     * @param value the value to assign
     * @return a reference to the same object
     */
    OptionalField<T, IDX> &operator=(const boost::optional<T> &value)
    {
        v = value;
        return *this;
    };

    /** @brief Check if the field is set
     * @return true if set
     */
    bool isSet() const
    {
        return v.is_initialized();
    }

    /** @brief Check if the field is not set
     * @return true if not set
     */
    bool isUnset() const
    {
        return !v.is_initialized();
    }

    /**
     * @brief retrieve a reference to the underlying value. It throws an exception if it is not set.
     * @return a reference to the underlying value
     * @throws an exception if the value is not set
     */
    T &value()
    {
        return v.value();
    }

    /**
     * @brief retrieve a const reference to the underlying value. It throws an exception if it is not set.
     * @return a const reference to the underlying value
     * @throws an exception if the value is not set
     */
    const T &value() const
    {
        return v.value();
    }

    /**
     * @brief unsets the underlying value, making it not initialized.
     * @return
     */
    OptionalField<T, IDX> &unset()
    {
        v.reset();
        return *this;
    };

    const boost::optional<T> &getRef() const
    {
        return v;
    }

    boost::optional<T> &getRef()
    {
        return v;
    }
};

/**
 * @brief An helper class to serialize the Optional fields in a single bitmap array
 * @tparam N number of bits to serialize
 *
 * @code
    utils::OptionalField<int,0> option1;
    utils::OptionalField<StrongType<int,Option2Tag>,1> option2;

    // Parse two optional fields

    utils::OptionalFieldBitmap<2> bmIn{option1, option2};
    bmIn.read(stream) >> option1 >> option2;

    // formats
    utils::OptionalFieldBitmap<2> bmOut{option1, option2};
    stream << bmOut << option1 << option2;
 * @endcode
 */
template<int N>
class OptionalFieldBitmap {
    static constexpr int ARRAYSIZE = (N - 1) / 8 + 1;
    std::array<uint8_t, ARRAYSIZE> bm = {};

    template<int IDX, typename ...FIELDS, typename std::enable_if<IDX < sizeof...(FIELDS), int>::type = 0>
    void assign_impl(std::tuple<FIELDS...> fields)
    {
        assign(std::get<IDX>(fields));
        assign_impl<IDX + 1>(fields);
    };

    template<int IDX, typename ...FIELDS, typename std::enable_if<IDX == sizeof...(FIELDS), int>::type = 0>
    void assign_impl(std::tuple<FIELDS...>)
    {
    };
public:
    /**
     * @brief Default constructor
     */
    OptionalFieldBitmap() : bm()
    {}

    /**
     * @brief Constructor with a list of optional fields, that initialize the internal bitmap structure
     * The state of the internal bitmap is updated with the set/notset state of the relative field, so for each
     * field that is set the bit is set accordingly.
     * Please note that if later on the field state is changed, the bitmap state is NOT set. use assing() to
     * reconcile the state instead.
     * @tparam FIELDS the list of fields
     * @param fields the list of fields
     */
    template<typename ...FIELDS>
    OptionalFieldBitmap(FIELDS... fields)
    {
        assign_impl<0>(std::make_tuple(fields...));
    }

    /**
     * @brief Assigns the state of an optional field with the relative bit in the bitmap .
     * @tparam T
     * @tparam IDX
     * @param field
     * @return the object itself
     */
    template<typename T, int IDX>
    OptionalFieldBitmap &assign(const OptionalField<T, IDX> &field)
    {
        constexpr int cidx = IDX / 8;
        constexpr uint8_t bit = IDX % 8;

        if (field.isSet()) {
            bm[cidx] |= (1 << bit);
        } else {
            bm[cidx] &= ~(1 << bit);
        }
        return *this;
    };

    /**
     * @brief check the state of the field according to the INTERNAL state of the bitmap, NOT with the field itself
     * @tparam T
     * @tparam IDX
     * @param field
     * @return true if the bitmap has the bit set for the field, false if not
     */
    template<typename T, int IDX>
    bool isSet(const OptionalField<T, IDX> &field) const
    {
        constexpr int cidx = IDX / 8;
        constexpr uint8_t bit = IDX % 8;

        return (bm[cidx] & (1 << bit)) != 0;
    };

    /**
     * @brief Access the internal value of the bitmap container as a reference
     * @param idx The index of the byte to access
     * @return the value of the bitmap byte
     */
    uint8_t &operator[](int idx)
    {
        return bm[idx];
    }

    /**
     * @brief Access the internal value of the bitmap container as a const reference
     * @param idx The index of the byte to access
     * @return the value of the bitmap byte
     */
    const uint8_t &operator[](int idx) const
    {
        return bm[idx];
    }

    /**
     * @brief Gets a copy of the internal value of the bitmap container
     * @param idx The index of the byte to access
     * @return the value of the bitmap byte
     */
    uint8_t get(int idx)
    {
        return bm[idx];
    }

    /**
     * @brief Gets the number of bytes of the internal bitmap container.
     * @return the number of bytes of the internal bitmap container.
     */
    constexpr int size() const
    { return ARRAYSIZE; }

    /**
     * @brief An helper class to serialize the optional fields from an InputDatStream.
     *
     * This class is initialized with a field bitmap, and an internal counter keeps track of the used fields.
     * @tparam Endianness
     */
    template<typename Endianness>
    class InputDataStreamHelper {
        int it = 0;
        const OptionalFieldBitmap bitmap;
        InputDataStream <Endianness> &stream;
    public:
        InputDataStreamHelper(const OptionalFieldBitmap &fieldBitmap, InputDataStream <Endianness> &inputDataStream) :
                bitmap(fieldBitmap),
                stream(inputDataStream)
        {}

        /**
         * @brief Input from stream operator.
         * @tparam T
         * @tparam IDX
         * @param field
         * @return
         */
        template<typename T, int IDX>
        InputDataStreamHelper &operator>>(OptionalField<T, IDX> &field)
        {
            if (IDX != it) {
                std::ostringstream ss;
                ss << "Out of Index OptionalField, Field@" << IDX << " pos: " << it;
                throw std::logic_error(ss.str());
            }
            if (bitmap.isSet(field)) {
                T v;
                stream >> v;
                field = v;
            } else {
                field.unset();
            }
            ++it;
            return *this;
        }
    };

    /**
     * @brief Create an instance of the InputDataStreamHelper class to allow serializing optional fields from a Data
     * Stream
     * @tparam Endianness
     * @param os
     * @return an instance of InputDataStreamHelper
     */
    template<typename Endianness>
    inline InputDataStreamHelper<Endianness> read(InputDataStream <Endianness> &os)
    {
        for (size_t i = 0; i < size(); ++i) {
            os >> bm[i];
        }
        InputDataStreamHelper<Endianness> stream(*this, os);
        return stream;
    }
};

/**
 * @brief Serialize the OptionalFieldBitmap object to a DataStream stream.
 * @tparam Endianness
 * @tparam N
 * @param os
 * @param fieldBitmap
 * @return
 */
template<typename Endianness, int N>
inline OutputDataStream <Endianness> &
operator<<(OutputDataStream <Endianness> &os, const OptionalFieldBitmap<N> &fieldBitmap)
{
    for (size_t i = 0; i < fieldBitmap.size(); ++i)
        os << fieldBitmap[i];
    return os;
}

/**
 * @brief Serialize an OptionalField to an OutputDataStream IF and only if the stream is set. @see OptionalFieldBitmap
 * about how to use.
 * @tparam Endianness
 * @tparam T
 * @tparam IDX
 * @param os
 * @param field
 * @return
 */
template<typename Endianness, typename T, int IDX>
inline OutputDataStream <Endianness> &operator<<(OutputDataStream <Endianness> &os, const OptionalField<T, IDX> &field)
{
    if (field.isSet()) {
        os << field.value();
    }
    return os;
};

}


#endif //BRIDGEMATE3_OPTIONALFIELDS_H
