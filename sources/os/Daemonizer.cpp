/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 26/01/17
 */

#include "Daemonizer.h"

#include <boost/log/trivial.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/syslog_backend.hpp>
#include <boost/shared_ptr.hpp>

#include <cstring>
#include <cerrno>
#include <csignal>

#if !defined(_WIN32)

#include <unistd.h>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/fcntl.h>

#endif

using namespace sys;
namespace bl = boost::log;

struct sys::Daemonizer::Impl {
    std::string mProgName;
    bool mEnabled = true;
    bool mKill = false;
    bool mCloseStdinout = true;

#if !defined (_WIN32)
    pid_t pid = -1;
#endif

};

Daemonizer::Daemonizer(const char *name)
        : p(std::make_unique<Impl>())
{
    p->mProgName = name;
}

Daemonizer::~Daemonizer() noexcept = default;

bool Daemonizer::run()
{
    if (p->mEnabled) {
#if !defined(_WIN32)
        auto pid = fork();
        if (pid < 0) {
            BOOST_LOG_TRIVIAL(error) << "Cannot Fork(1): " << strerror(errno) << "\n";
            return false;
        }

        if (pid == 0) { // Forked, shut down parent process.
            typedef bl::sinks::synchronous_sink<bl::sinks::syslog_backend> sink_t;

            boost::shared_ptr<bl::sinks::syslog_backend> backend(new bl::sinks::syslog_backend(
                    bl::keywords::facility = bl::sinks::syslog::user,
                    bl::keywords::use_impl = bl::sinks::syslog::native
            ));

            // Set the straightforward level translator for the "Severity" attribute of type int
            backend->set_severity_mapper(bl::sinks::syslog::direct_severity_mapping<int>("Severity"));

            // Wrap it into the frontend and register in the core.
            // The backend requires synchronization in the frontend.
            bl::core::get()->add_sink(boost::make_shared<sink_t>(backend));

            if (setsid() < 0) {
                BOOST_LOG_TRIVIAL(error) << "Cannot set session id: " << strerror(errno) << "\n";
                return false;
            }

            pid = fork();
            if (pid < 0) {
                BOOST_LOG_TRIVIAL(error) << "Cannot Fork(2): " << strerror(errno) << "\n";
                return false;
            }

            if (pid > 0) { // Forked again, shut down parent process.
                exit(EXIT_SUCCESS);
            }

            umask(0);

            if (p->mCloseStdinout) {
                int devnull = ::open("/dev/null", O_RDWR);
                assert(devnull != -1);

                int r = dup2(devnull, STDIN_FILENO);
                assert(r != -1);
                r = dup2(devnull, STDOUT_FILENO);
                assert(r != -1);
                r = dup2(devnull, STDERR_FILENO);
                assert(r != -1);
            }
        } else {
            exit(0);
        }
#endif
    }
    return true;
}

void Daemonizer::setEnabled(bool enabled)
{
    p->mEnabled = enabled;
}

void Daemonizer::setKill(bool kill)
{
    p->mKill = kill;
}
