/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 26/01/17
 */

#ifndef NSSRFID_PROJECT_DAEMONIZER_H
#define NSSRFID_PROJECT_DAEMONIZER_H

#include <exception>
#include <sys/types.h>
#include <string>
#include <memory>

namespace sys {

class Daemonizer {
    struct Impl;
    std::unique_ptr<Impl> p;
public:
    class StartFailed : public std::exception {
    };

    /** @brief exception to allow the main app to exit cleaning
     *
     */
    class Exited : public std::exception {
    };

    /** @brief Construct the daemonizer
     *
     * @param name The program name, taken from argv[0]
     * @throws StartFailed when a problem occurs, Exited when the main app should cleanly exiting
     */
    explicit Daemonizer(const char *name);

    ~Daemonizer() noexcept;

    void setEnabled(bool enabled);

    void setKill(bool kill);

    bool run();
};

}

#endif //NSSRFID_PROJECT_DAEMONIZER_H
