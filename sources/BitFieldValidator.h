/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 17/01/19
 */

#ifndef GUTILS_BITFIELDVALIDATOR_H
#define GUTILS_BITFIELDVALIDATOR_H

#include "BitStream.h"

#include <type_traits>

namespace utils {

template<typename Type, typename Validator>
struct ValidatedType : Type, Validator {
    using Validator::validate;
};

template<typename Type, typename Validator>
InBitStream &operator >> (InBitStream &stream, ValidatedType<Type, Validator> &v) {
    stream >> static_cast<Type &>(v);
    v.validate(v);
    return stream;
}

}

#endif //GUTILS_BITFIELDVALIDATOR_H
