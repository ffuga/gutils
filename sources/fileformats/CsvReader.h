/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 02/02/18
 * @addtogroup Bm3Utils
 * @{
 */

#ifndef BRIDGEMATE3_CSVREADER_H
#define BRIDGEMATE3_CSVREADER_H

#include <string>
#include <iostream>
#include <vector>
#include <tuple>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace utils {
namespace fileformats {

/**
 * @brief A parser for CSV files
 * @tparam Ts A list of types for fields
 *
 * This class allows client code to convert a line of text formatted as a CSV, into a tuple of strong types, separated
 * by a proper separator.
 *
 * It can be used this way:
 * @code
 *     utils::fileformats::CsvLineParser<int, int, int, float, float, int> lp;
 *     int linenum = 0;
 *     std::string line;
 *     while (infile) {
 *        std::getline(infile, line);
 *        boost::trim(line);
 *        if (line.empty())
 *            continue;
 *        auto v = lp(line, ",", linenum++);
 *        auto firstvalue = std::get<0>(v);
 *        // ... and so on...
 * @endcode
 *
 */
template<typename ...Ts>
class CsvLineParser {

    template<std::size_t I = 0>
    inline typename std::enable_if<I == sizeof...(Ts), void>::type
    parse(std::tuple<Ts...> &tuple, const std::vector<std::string> &vec)
    {
        (void) tuple;
        (void) vec;
    };

    template<std::size_t I = 0>
    inline typename std::enable_if<I < sizeof...(Ts), void>::type
    parse(std::tuple<Ts...> &tuple, const std::vector<std::string> &vec)
    {
        try {
            if (!vec[I].empty()) {
                std::get<I>(tuple) = boost::lexical_cast<typename std::tuple_element<I, std::tuple<Ts...> >::type>(
                        vec[I]);
            }
        } catch (boost::bad_lexical_cast &x) {
            std::ostringstream ss;
            ss << x.what() << " fld: " << I << " (" << vec[I] << ")";
            throw std::runtime_error(ss.str());
        }

        parse<I + 1>(tuple, vec);
    };

public:
    /**
     * @brief Parses the passed line, converting it in the proper tuple of types (Ts)
     * @param line The text to convert
     * @param separator a list of separator
     * @param linenum the reference number for that line (line number on a file, for example)
     * @return a std::tuple<Ts...> of Ts with proper converted values
     * @throws std::runtime_error with an error message and indication of the offending field.
     */
    std::tuple<Ts...> operator()(std::string line, const char *separator, int linenum = 0)
    {
        std::vector<std::string> sr;
        boost::split(sr, line, boost::is_any_of(separator));

        std::tuple<Ts...> values;
        try {
            parse(values, sr);
        } catch (std::exception &x) {
            std::ostringstream ss;
            ss << x.what() << " line: " << linenum + 1;
            throw std::runtime_error(ss.str());
        }

        return values;
    }
};

}
}

/// @}

#endif //BRIDGEMATE3_CSVREADER_H
