//
// Created by Federico Fuga on 19/11/18.
//

#ifndef BRIDGEMATE3_BITSTREAM_H
#define BRIDGEMATE3_BITSTREAM_H

#include "BitField.h"

#include <vector>
#include <iostream>
#include <functional>
#include <algorithm>

namespace utils {

class OutBitStream {
    static constexpr int NoFlags = 0;
    static constexpr int SkipUnaligned = 0x01;

    int mFlags = NoFlags;

    std::vector<uint8_t> &buffer;
    uint8_t bitCount{0};

    uint8_t freespace() const
    {
        return bitCount;
    }

    void pushLowerBits(uint8_t bits, uint8_t byte)
    {
        if (freespace() == 0) {
            buffer.push_back(0);
            bitCount = 8;
        }
        buffer.back() = (buffer.back() | (byte << (freespace() - bits)));
        bitCount -= bits;
    }

    /// @brief push the high n bits of the byte
    void pushHigh(uint8_t bits, uint8_t byte)
    {
        uint8_t value = (byte >> (8 - bits)) & 0xff;
        pushLowerBits(bits, value);
    }


    /// @brief push the low n bits of the byte
    void pushLow(uint8_t bits, uint8_t byte)
    {
        uint8_t mask = static_cast<uint8_t >(uint16_t((1 << (bits)) - 1) & 0xff);
        uint8_t value = (byte) & mask;
        pushLowerBits(bits, value);
    }

    // Always use this accessor.
    void pushField(uint8_t bits, uint8_t byte)
    {
        if (bits == 0) {
            return;
        }

        uint8_t avail = freespace();
        if (avail == 0) {
            avail = 8;
        }      // a new byte will be available
        int n = std::min(bits, avail);
        pushHigh(n, (byte << (8 - bits)));
        n = bits - n;
        if (n > 0) {
            pushLow(n, byte);
        }
    }

public:
    explicit OutBitStream(std::vector<uint8_t> &outbuffer) : buffer(outbuffer)
    {}

    const std::vector<uint8_t> &bytes() const
    { return buffer; }

    void skipUnaligned()
    {
        mFlags = mFlags | SkipUnaligned;
    }

    OutBitStream &push(std::vector<uint8_t> const &data)
    {
        if (bitCount > 0) {
            if (!(mFlags & OutBitStream::SkipUnaligned)) {
                throw std::runtime_error("Unaligned push to OutBitStream");
            }
            bitCount = 0;
        }
        std::copy(data.begin(), data.end(), std::back_inserter(buffer));
        return *this;
    }

    friend OutBitStream &operator<<(OutBitStream &bs, uint8_t byte)
    {
        bs.pushField(8, byte);
        return bs;
    }

    template<int N, typename C>
    friend OutBitStream &operator<<(OutBitStream &bs, const BitField <N, C> &v)
    {
        uint8_t firstbit = ((N - 1) % 8) + 1;
        uint8_t minbytes = ((N - 1) / 8 + 1);
        int firstbyte = (sizeof(C) - minbytes);
        for (auto i = firstbyte; i < sizeof(C); ++i) {
            uint8_t b = (static_cast<int>(v.value()) >> (8 * (sizeof(C) - i - 1))) & 0xff;
            bs.pushField(firstbit, b);
            firstbit = 8;
        }
        return bs;
    }


    friend OutBitStream &operator<<(OutBitStream &os, bool b)
    {
        os << BitField<1>(b ? 1 : 0);
        return os;
    }

    friend OutBitStream &operator<<(OutBitStream &os, std::vector<uint8_t> const &data)
    {
        return os.push(data);
    }
};

class InBitStream {
    static constexpr int NoFlags = 0;
    static constexpr int SkipUnaligned = 0x01;

    int mFlags = NoFlags;

    std::vector<uint8_t>::const_iterator iterator, end;
    uint8_t bitCount{8};

    uint8_t getAvailableBitsOnThisByte() const
    {
        return bitCount;
    }

    int getNextBits(uint8_t bitnum)
    {
        int r = 0;
        while (bitnum > 0) {
            if (atEnd()) {
                throw InputPastEndOverflow();
            }
            uint8_t avail = getAvailableBitsOnThisByte();
            int n = std::min(avail, bitnum);

            auto mask = static_cast<uint8_t >((1 << (n)) - 1);
            uint8_t v = (*iterator >> (bitCount - n)) & mask;
            r |= (v << (bitnum - n));

            bitnum -= n;
            bitCount -= n;
            if (bitCount == 0) {
                bitCount = 8;
                ++iterator;
            }
        }
        return r;
    }

public:
    // FIXME InputPastEnd should derive from std::out_of_range
    class InputPastEndOverflow : public std::exception {
        using std::exception::exception;
    };

    class IllegalValueException : public std::invalid_argument {
        using std::invalid_argument::invalid_argument;
    };

    explicit InBitStream(const std::vector<uint8_t> &data)
            : iterator(data.begin()), end(data.end())
    {

    }

    void skipUnaligned()
    {
        mFlags |= SkipUnaligned;
    }

    std::vector<uint8_t> remainingBytes()
    {
        std::vector<uint8_t> rem;
        if (getAvailableBitsOnThisByte() < 8) {
            if (!(mFlags & SkipUnaligned)) {
                throw std::runtime_error("Unaligned read while reading block from InBitStream");
            }
            // skip to the next
            getNextBits(getAvailableBitsOnThisByte());
        }
        std::copy(iterator, end, std::back_inserter(rem));
        iterator = end;
        return rem;
    }

    bool atEnd() const
    { return iterator == end; }

    bool operator!() const
    { return !atEnd(); }

    template<typename T>
    void pick(T &&t)
    {
        auto save_it = iterator;
        auto save_bc = bitCount;

        try {
            (*this) >> t;
        } catch (...) {
            bitCount = save_bc;
            iterator = save_it;
            throw;
        }

        bitCount = save_bc;
        iterator = save_it;
    }

    friend InBitStream &operator>>(InBitStream &bs, uint8_t &byte)
    {
        BitField<8> b;
        bs >> b;
        byte = b.value();
        return bs;
    }

    template<int N, typename C>
    friend InBitStream &operator>>(InBitStream &is, BitField <N, C> &f)
    {
        using CR = typename std::remove_reference<C>::type;
        f.value() = CR(is.getNextBits(N));
        return is;
    }

    template<int N, typename C>
    friend InBitStream &operator>>(InBitStream &is, BitField<N, C &> &&f)
    {
        using CR = typename std::remove_reference<C>::type;
        f.value() = CR(is.getNextBits(N));
        return is;
    }

    friend InBitStream &operator>>(InBitStream &is, bool &b)
    {
        b = (is.getNextBits(1) == 1);
        return is;
    }
};

template<int N>
class ReservedBits {
public:
    ReservedBits() = default;

    friend InBitStream &operator>>(InBitStream &stream, ReservedBits<N> s)
    {
        int r;
        stream >> makeBitFieldRef<N>(r);
        if (r != 0) {
            throw InBitStream::IllegalValueException("Reserved bit set");
        }
        return stream;
    }

    friend OutBitStream &operator<<(OutBitStream &stream, ReservedBits<N> s)
    {
        stream << makeBitField<N>(0);
        return stream;
    }
};


}

#endif //BRIDGEMATE3_BITSTREAM_H
