//
// Created by happycactus on 16/11/18.
//

#ifndef BRIDGEMATE3_ENDIANNESTYPES_H
#define BRIDGEMATE3_ENDIANNESTYPES_H

namespace utils {
struct BigEndian {
};
struct LittleEndian {
};
using NetworkOrdering = BigEndian;
}

#endif //BRIDGEMATE3_ENDIANNESTYPES_H
