/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_ENUM_BITSTREAMTYPES_H
#define GUTILS_ENUM_BITSTREAMTYPES_H

#include "Enum.h"

namespace utils {

template<int N, typename TYPE, typename TAG>
struct BitAwareEnum : Enum<TYPE, TAG> {
    BitAwareEnum() {}

    BitAwareEnum(TYPE t) : Enum<TYPE, TAG>(t)
    {}

    BitAwareEnum(BitAwareEnum<N, TYPE, TAG> const &t) = default;

    BitAwareEnum(BitAwareEnum<N, TYPE, TAG> &&t) = default;

    BitAwareEnum<N, TYPE, TAG> &operator=(BitAwareEnum<N, TYPE, TAG> &&t) = default;

    BitAwareEnum<N, TYPE, TAG> &operator=(BitAwareEnum<N, TYPE, TAG> const &t) = default;


    BitAwareEnum<N, TYPE, TAG> operator=(TYPE const &o)
    {
        Enum<TYPE, TAG>::operator=(o);
        return *this;
    }

    BitAwareEnum<N, TYPE, TAG> operator=(TYPE &&o)
    {
        Enum<TYPE, TAG>::operator=(o);
        return *this;
    }
};

}

#endif //GUTILS_BITSTREAMTYPES_H
