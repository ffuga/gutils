/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_ENUM_BITSTREAMOPERATIONS_H
#define GUTILS_ENUM_BITSTREAMOPERATIONS_H

#include "BitStream.h"
#include "Enum/BitStreamTypes.h"

namespace utils {

template<int N, typename Type, typename Tag>
utils::OutBitStream &operator<<(utils::OutBitStream &stream, BitAwareEnum<N, Type, Tag> const &en)
{
    return (stream << utils::makeBitField<N>(en.value()));
}

template<int N, typename Type, typename Tag>
utils::InBitStream &operator>>(utils::InBitStream &stream, BitAwareEnum<N, Type, Tag> &en)
{
    Type v;
    stream >> utils::makeBitFieldRef<N>(v);
    en = BitAwareEnum<N, Type, Tag>{v};
    return stream;
}


}

#endif //GUTILS_ENUM_BITSTREAMOPERATIONS_H
