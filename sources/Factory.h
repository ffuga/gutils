//
// Created by Federico Fuga on 07/03/18.
//

#ifndef BRIDGEMATE3_FACTORY_H
#define BRIDGEMATE3_FACTORY_H

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

#include <map>
#include <functional>
#include <sstream>
#include <vector>
#include <regex>

#ifndef _MSC_VER
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

template<typename K, typename V, typename Matcher>
class FactoryEasyInitializer;

template<typename T, template<typename> class PTR, typename ...ARGS>
using FactoryFunction = std::function<PTR<T>(ARGS...)>;

template<typename K, typename V>
struct SimpleFactoryMatcher {
    using Map = std::map<K, V>;
    Map map;

    auto operator()(const K &key) const
    {
        return map.find(key);
    }

    void insert(std::pair<K, V> pair)
    {
        map.insert(std::move(pair));
    }
};

template<typename K, typename S, typename V>
struct RegexFactoryMatcher {
    using Map = std::vector<std::pair<K, V>>;
    Map map;

    auto operator()(const S &key) const
    {
        return find_if(map.begin(), map.end(), [&key](auto entry) {
            return regex_match(key, std::get<0>(entry));
        });
    }

    void insert(std::pair<K, V> pair)
    {
        map.push_back(pair);
    }
};

class FactoryKeyNotFoundException : public std::exception {
    std::string err;
public:
    template<typename S>
    explicit FactoryKeyNotFoundException(const S &notFoundKey)
    {
        std::ostringstream ss;
        ss << "Key not found: " << notFoundKey;
        err = ss.str();
    };

    const char *what() const NOEXCEPT override
    {
        return err.c_str();
    }
};

template<typename K, typename V, typename Matcher = SimpleFactoryMatcher<K, V>>
class Factory {
public:
private:
    Matcher map;
public:
    Factory() : map()
    {}

    FactoryEasyInitializer<K, V, Matcher> addFactory()
    {
        return FactoryEasyInitializer<K, V, Matcher>(*this);
    };

    void add(K key, V func)
    {
        map.insert(std::make_pair(key, func));
    }

    template<typename S, typename ...ARGS>
    auto create(S key, ARGS... args)
    {
        auto f = map(key);
        if (f == map.map.end()) {
            throw FactoryKeyNotFoundException(key);
        }

        return f->second(args...);
    }
};

template<typename K, typename V, typename Matcher>
class FactoryEasyInitializer {
    Factory<K, V, Matcher> &factory;
public:
    explicit FactoryEasyInitializer(Factory<K, V, Matcher> &factoryRef)
            : factory(factoryRef)
    {
    }

    FactoryEasyInitializer &operator()(K key, V function)
    {
        factory.add(key, function);
        return *this;
    }
};


#endif //BRIDGEMATE3_FACTORY_H
