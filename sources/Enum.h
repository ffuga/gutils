/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_ENUM_H
#define GUTILS_ENUM_H

#include <type_traits>

namespace utils {

template<typename T, typename TAG>
class Enum {
    T mValue;
public:
    Enum() : mValue()
    {};

    Enum(T v)
            : mValue(v)
    {

    }

    using Enums = T;
    using Type = TAG;

    const T &value() const
    { return mValue; }

    T &value()
    { return mValue; }

    auto underlyingValue() const
    {
        return static_cast<typename std::underlying_type<T>::type> (mValue);
    }

    friend bool operator==(Enum<T, TAG> lh, Enum<T, TAG> rh)
    {
        return lh.value() == rh.value();
    }

    friend bool operator!=(Enum<T, TAG> lh, Enum<T, TAG> rh)
    {
        return (lh.value() != rh.value());
    }

    friend bool operator==(Enum<T, TAG> lh, T rh)
    {
        return lh.value() == rh;
    }

    friend bool operator!=(Enum<T, TAG> lh, T rh)
    {
        return (lh.value() != rh);
    }

    friend bool operator==(T lh, Enum<T, TAG> rh)
    {
        return lh == rh.value();
    }

    friend bool operator!=(T lh, Enum<T, TAG> rh)
    {
        return lh != rh.value();
    }
};


}
#endif //GUTILS_ENUM_H
