/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_STRONGTYPE_BITSTREAMOPERATIONS_H
#define GUTILS_STRONGTYPE_BITSTREAMOPERATIONS_H

#include "BitStream.h"
#include "StrongType/BitStreamTypes.h"

namespace utils {

template<int N, typename T, typename TAG, typename Validator>
OutBitStream &operator<<(OutBitStream &os, BitField<N, utils::StrongType<T, TAG, Validator> &> &stf)
{
    os << BitField<N, T>(stf.value().value());
    return os;
}

template<int N, typename T, typename TAG, typename Validator>
OutBitStream &operator<<(OutBitStream &os, BitField<N, utils::StrongType<T, TAG, Validator> &> &&stf)
{
    os << BitField<N, T>(std::move(stf.value().value()));
    return os;
}

template<int N, typename Type, typename Tag, typename Validator>
utils::OutBitStream &operator<<(utils::OutBitStream &stream, BitAwareStrongType<N, Type, Tag, Validator> const &en)
{
    return (stream << utils::makeBitField<N>(en.value()));
}

template<int N, typename Type, typename Tag, typename Validator>
utils::InBitStream &operator>>(utils::InBitStream &stream, BitAwareStrongType<N, Type, Tag, Validator> &en)
{
    return (stream >> utils::makeBitFieldRef<N>(en));
}


}

#endif //GUTILS_STRONGTYPE_BITSTREAMOPERATIONS_H
