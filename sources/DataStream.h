/**
 * @file
 * @author Federico Fuga <fuga@studiofuga.com>
 * @addtogroup Bm3Utils
 * @{
 */

/*
Copyright (C) 2017 Federico Fuga <fuga@studiofuga.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DATASTREAM_H
#define DATASTREAM_H

#include "EndiannesTypes.h"

#include <vector>
#include <array>
#include <string>

namespace utils {
using VectorLenghtType = uint16_t;    // note: see also DataStreamCommons.h

/**
 * @brief An exception indicating that an operation on DataStream as overflown the buffer.
 * This can mean that an Output buffer is full or the Input buffer is empty.
 */
class StreamOutOfBoundException : public std::runtime_error {
public:
    StreamOutOfBoundException() : std::runtime_error("Stream Out of bound")
    {}
};

/**
 * @brief A Base class for managing strong typed input of DataStreams
 * This class is not to be used directly, but through the InputDataStream class.
 * This class implements just a couple of basic operation like pop() to extract data from the stream.
 * Operations with Strong Types and Endianness are implemented in InputDataStream.
 */
class BaseInputDataStream {
protected:
    const std::vector<uint8_t> &container;
    std::vector<uint8_t>::const_iterator iterator;

    /**
     * @brief Extracts a byte from the buffer
     * @return the extracted byte
     * @throws StreamOutOfBoundException if there aren't any byte to extract
     */
    uint8_t pop()
    {
        if (iterator == container.end()) {
            throw StreamOutOfBoundException();
        }

        auto r = *iterator;
        ++iterator;
        return r;
    }

public:
    /**
     * @brief Constructor for the Stream using a vector<uint8_t> as input buffer.
     * @param pkt the input buffer to use
     */
    explicit BaseInputDataStream(const std::vector<uint8_t> &pkt)
            : container(pkt), iterator(container.begin())
    {
    }

    ~BaseInputDataStream() noexcept = default;

    BaseInputDataStream(const BaseInputDataStream &) = delete;

    BaseInputDataStream(BaseInputDataStream &&) = default;

    BaseInputDataStream &operator=(const BaseInputDataStream &) = delete;

    BaseInputDataStream &operator=(BaseInputDataStream &&) = default;

    /**
     * @brief Checks if the container buffer is empty or not
     * @return true if the container is empty, false otherwise (bytes can be extracted)
     */
    bool atEnd() const
    { return iterator == container.end(); }

    /**
     * @brief Checks the number of remaining bytes in the buffer
     * @return returns the number of bytes in the buffer
     */
    size_t remaining() const
    { return std::distance(iterator, container.end()); }

    /**
     * @brief Extract N bytes from the buffer and puts them in a std::array
     * @tparam N The length of the array
     * @return a std::array<uint8_t,N>
     * @throws StreamOutOfBoundException if there aren't enough bytes in the buffer
     */
    template<int N>
    inline std::array<uint8_t, N> getArray()
    {
        std::array<uint8_t, N> a;
        for (int i = 0; i < N; ++i)
            a[i] = pop();
        return a;
    }

    /**
     * @brief Extracts n bytes from the buffer and puts them in a std::vector
     * @param n The number of bytes to extract
     * @return the extracted vector
     * @throws StreamOutOfBoundException if there aren't enough bytes in the buffer
     */
    inline std::vector<uint8_t> getVector(int n)
    {
        std::vector<uint8_t> a;
        for (int i = 0; i < n; ++i)
            a.emplace_back(pop());
        return a;
    }

    /**
     * @brief Extracts n bytes from the buffer and puts them in a std::string
     * @param n The number of bytes to extract
     * @return the extracted string
     * @throws StreamOutOfBoundException if there aren't enough bytes in the buffer
     */
    inline std::string getString(int n)
    {
        std::string a;
        for (int i = 0; i < n; ++i)
            a.push_back(pop());
        return a;
    }

    /**
     * @brief Extracts all the bytes available from the buffer and puts them in a std::vector
     * @return the extracted vector
     */
    std::vector<uint8_t> getAll()
    {
        std::vector<uint8_t> v;
        while (!atEnd()) {
            v.emplace_back(pop());
        }
        return v;
    }
};

class BaseOutputDataStream {
protected:
    std::vector<uint8_t> &packet;

public:
    explicit BaseOutputDataStream(std::vector<uint8_t> &pkt)
            : packet(pkt)
    {
    }

    template<template<typename, typename> class C, typename U, typename V>
    inline void push(const C<U, V> &array)
    {
        for (auto x : array)
            push(x);
    }

    template<typename T>
    inline void push(const T &v)
    {
        packet.push_back(v);
    }

    template<class ForwardIterator>
    inline void push(ForwardIterator first, ForwardIterator last)
    {
        while (first != last) {
            push(*first);
            ++first;
        }
    };
};

/**
 * @brief A Data stream that can recover structured data from a buffer
 * @tparam Endianness The endianness of the stream, @sa BigEndian @sa LittelEndian @sa NetworkOrdering
 */
template<typename Endianness>
class InputDataStream : public BaseInputDataStream {
    template<typename T>
    void getRef(T &t)
    {
        t = get<T>();
    }

public:
    using BaseInputDataStream::BaseInputDataStream;

    ~InputDataStream() noexcept = default;

    InputDataStream(const InputDataStream &) = delete;

    InputDataStream(InputDataStream &&) = default;

    InputDataStream &operator=(const InputDataStream &) = delete;

    InputDataStream &operator=(InputDataStream &&) = default;


    /**
     * @brief Recover a string from the buffer, with a length indicator
     * @tparam LenType the type of length to use
     * @return the string
     */
    template<typename LenType>
    std::string getString()
    {
        std::string s;
        LenType len;
        getRef(len);
        for (size_t i = 0; i < len; ++i)
            s.push_back(pop());
        return s;
    }

    /**
     * @brief Recovers a type from the buffer
     * This is a deleted function, because this function is intended to be specialized through the T type.
     * @tparam T the type to recover
     * @return the value
     */
    template<typename T>
    inline T get()
#if defined (__MACH__)
    {
        static_assert(sizeof(T) == 0, "Only specializations of get can be used");
    }
#else
    = delete;

#endif

};

template<typename Endianness>
class OutputDataStream : public BaseOutputDataStream {
public:
    using BaseOutputDataStream::BaseOutputDataStream;

    template<typename T>
    inline void put(const T &)
#if defined (__MACH__)
    {
        static_assert(sizeof(T) == 0, "Only specializations of get can be used");
    }
#else
    = delete;

#endif

    template<typename LenType>
    void putString(const std::string &s)
    {
        LenType len = s.size();
        put(len);
        push(s.begin(), s.end());
    }
};

/**
 * @brief Specializes the get() function for uint8_t and BigEndian
 * @return the recovered value
 */
template<>
template<>
inline uint8_t InputDataStream<BigEndian>::get<uint8_t>()
{
    return pop();
}

/**
 * @brief Specializes the get() function for uint8_t and LittleEndian
 * @return the recovered value
 */
template<>
template<>
inline uint8_t InputDataStream<LittleEndian>::get<uint8_t>()
{
    return pop();
}

template<>
template<>
inline bool InputDataStream<LittleEndian>::get<bool>()
{
    return (pop() != 0);
}

template<>
template<>
inline bool InputDataStream<BigEndian>::get<bool>()
{
    return (pop() != 0);
}


template<>
template<>
inline void OutputDataStream<BigEndian>::put<uint8_t>(const uint8_t &v)
{
    push(v);
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<uint8_t>(const uint8_t &v)
{
    push(v);
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<bool>(const bool &v)
{
    push(v ? 1 : 0);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<bool>(const bool &v)
{
    push(v ? 1 : 0);
}


/**
 * @brief Specializes the get() function for int8_t and BigEndian
 * @return the recovered value
 */
template<>
template<>
inline int8_t InputDataStream<BigEndian>::get<int8_t>()
{
    return static_cast<int8_t>(pop());
}

/**
 * @brief Specializes the get() function for uint16_t and LittleEndian
 * @return the recovered value
 */
template<>
template<>
inline int8_t InputDataStream<LittleEndian>::get<int8_t>()
{
    return static_cast<int8_t>(pop());
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<int8_t>(const int8_t &v)
{
    push(static_cast<uint8_t>(v));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<int8_t>(const int8_t &v)
{
    push(static_cast<uint8_t>(v));
}

/**
 * @brief Specializes the get() function for uint16_t and BigEndian
 * @return the recovered value
 */
template<>
template<>
inline uint16_t InputDataStream<BigEndian>::get<uint16_t>()
{
    return static_cast<uint16_t> (pop() << 8) | (pop());
}

/**
 * @brief Specializes the get() function for uint16_t and LittleEndian
 * @return the recovered value
 */
template<>
template<>
inline uint16_t InputDataStream<LittleEndian>::get<uint16_t>()
{
    return static_cast<uint16_t>  (pop()) | (pop() << 8);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<uint16_t>(const uint16_t &v)
{
    push(v >> 8);
    push(v & 0x0ff);
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<uint16_t>(const uint16_t &v)
{
    push(v & 0x0ff);
    push(v >> 8);
}

/**
 * @brief Specializes the get() function for int16_t and BigEndian
 * @return the recovered value
 */
template<>
template<>
inline int16_t InputDataStream<BigEndian>::get<int16_t>()
{
    return static_cast<int16_t> (pop() << 8) | (pop());
}

/**
 * @brief Specializes the get() function for int16_t and LittleEndian
 * @return the recovered value
 */
template<>
template<>
inline int16_t InputDataStream<LittleEndian>::get<int16_t>()
{
    return static_cast<int16_t> (pop()) | (pop() << 8);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<int16_t>(const int16_t &v)
{
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v & 0x0ff));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<int16_t>(const int16_t &v)
{
    push(static_cast<uint8_t>(v & 0x0ff));
    push(static_cast<uint8_t>(v >> 8));
}

template<>
template<>
inline uint32_t InputDataStream<BigEndian>::get<uint32_t>()
{
    return static_cast<uint32_t>
           (pop() << 24) | (pop() << 16) | (pop() << 8) | (pop());
}

template<>
template<>
inline uint32_t InputDataStream<LittleEndian>::get<uint32_t>()
{
    return static_cast<uint32_t>
           (pop()) | (pop() << 8) | (pop() << 16) | (pop() << 24);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<uint32_t>(const uint32_t &v)
{
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v & 0x0ff));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<uint32_t>(const uint32_t &v)
{
    push(static_cast<uint8_t>(v & 0x0ff));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 24));
}

template<>
template<>
inline int32_t InputDataStream<BigEndian>::get<int32_t>()
{
    return static_cast<int32_t>
           (pop() << 24) | (pop() << 16) | (pop() << 8) | (pop());
}

template<>
template<>
inline int32_t InputDataStream<LittleEndian>::get<int32_t>()
{
    return static_cast<int32_t>
           (pop() << 0) | (pop() << 8) | (pop() << 16) | (pop() << 24);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<int32_t>(const int32_t &v)
{
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v & 0x0ff));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<int32_t>(const int32_t &v)
{
    push(static_cast<uint8_t>(v & 0x0ff));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 24));
}

template<>
template<>
inline int64_t InputDataStream<BigEndian>::get<int64_t>()
{
    return static_cast<int64_t>
           (static_cast<int64_t>(pop()) << 56) | (static_cast<int64_t>(pop()) << 48) |
           (static_cast<int64_t>(pop()) << 40) | (static_cast<int64_t>(pop()) << 32) |
           (static_cast<int64_t>(pop()) << 24) | (static_cast<int64_t>(pop()) << 16) |
           (static_cast<int64_t>(pop()) << 8) | (static_cast<int64_t>(pop()));
}

template<>
template<>
inline int64_t InputDataStream<LittleEndian>::get<int64_t>()
{
    return static_cast<int64_t>
           (static_cast<int64_t>(pop()) << 0) | (static_cast<int64_t>(pop()) << 8) |
           (static_cast<int64_t>(pop()) << 16) | (static_cast<int64_t>(pop()) << 24) |
           (static_cast<int64_t>(pop()) << 32) | (static_cast<int64_t>(pop()) << 40) |
           (static_cast<int64_t>(pop()) << 48) | (static_cast<int64_t>(pop()) << 56);
}

template<>
template<>
inline uint64_t InputDataStream<BigEndian>::get<uint64_t>()
{
    return static_cast<uint64_t>
           (static_cast<uint64_t>(pop()) << 56) | (static_cast<uint64_t>(pop()) << 48) |
           (static_cast<uint64_t>(pop()) << 40) | (static_cast<uint64_t>(pop()) << 32) |
           (static_cast<uint64_t>(pop()) << 24) | (static_cast<uint64_t>(pop()) << 16) |
           (static_cast<uint64_t>(pop()) << 8) | (static_cast<uint64_t>(pop()));
}

template<>
template<>
inline uint64_t InputDataStream<LittleEndian>::get<uint64_t>()
{
    return static_cast<uint64_t>
           (static_cast<uint64_t>(pop()) << 0) | (static_cast<uint64_t>(pop()) << 8) |
           (static_cast<uint64_t>(pop()) << 16) | (static_cast<uint64_t>(pop()) << 24) |
           (static_cast<uint64_t>(pop()) << 32) | (static_cast<uint64_t>(pop()) << 40) |
           (static_cast<uint64_t>(pop()) << 48) | (static_cast<uint64_t>(pop()) << 56);
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<int64_t>(const int64_t &v)
{
    push(static_cast<uint8_t>(v >> 56));
    push(static_cast<uint8_t>(v >> 48));
    push(static_cast<uint8_t>(v >> 40));
    push(static_cast<uint8_t>(v >> 32));
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v & 0x0ff));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<int64_t>(const int64_t &v)
{
    push(static_cast<uint8_t>(v & 0x0ff));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 32));
    push(static_cast<uint8_t>(v >> 40));
    push(static_cast<uint8_t>(v >> 48));
    push(static_cast<uint8_t>(v >> 56));
}

template<>
template<>
inline void OutputDataStream<BigEndian>::put<uint64_t>(const uint64_t &v)
{
    push(static_cast<uint8_t>(v >> 56));
    push(static_cast<uint8_t>(v >> 48));
    push(static_cast<uint8_t>(v >> 40));
    push(static_cast<uint8_t>(v >> 32));
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v & 0x0ff));
}

template<>
template<>
inline void OutputDataStream<LittleEndian>::put<uint64_t>(const uint64_t &v)
{
    push(static_cast<uint8_t>(v & 0x0ff));
    push(static_cast<uint8_t>(v >> 8));
    push(static_cast<uint8_t>(v >> 16));
    push(static_cast<uint8_t>(v >> 24));
    push(static_cast<uint8_t>(v >> 32));
    push(static_cast<uint8_t>(v >> 40));
    push(static_cast<uint8_t>(v >> 48));
    push(static_cast<uint8_t>(v >> 56));
}

template<typename Endianness, typename T, typename std::enable_if<!std::is_enum<T>::value, int>::type = 0>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const T &d)
{
    stream.template put<T>(d);
    return stream;
}

template<typename Endianness, typename T, typename std::enable_if<!std::is_enum<T>::value, int>::type = 0>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, T &d)
{
    d = stream.template get<T>();
    return stream;
}
};

/// @}

#endif //DATASTREAM_H